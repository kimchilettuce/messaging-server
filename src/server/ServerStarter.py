"""
    Sample code for Multi-Threaded Server
    Python 3
    Usage: python3 TCPserver3.py localhost 12000
    coding: utf-8
    
    Author: Wei Song (Tutor for COMP3331/9331)
"""
from socket import *
from typing import Dict, List, Optional
from threading import Thread

from src.Helper import printRecv, printPrompt, printSend, assertPTRCLMsg
from src.server.dbUsers import MsgBank, databaseClients

import sys, select, datetime, time

#region Constants
CREDENTIALS_FILE : str = "src/server/credentials.txt"
MAX_PASSWORD_ATTEMPT : int = 3
#endregion

# acquire server host and port from command line parameter
if len(sys.argv) != 4:
    print("\n===== Error usage, python3 ServerStarter.py SERVER_PORT BLOCK_DURATION TIMEOUT ======\n")
    exit(0)

serverHost = "127.0.0.1"
serverPort = int(sys.argv[1])
serverAddress = (serverHost, serverPort)

# define socket for the server side and bind address
serverSocket = socket(AF_INET, SOCK_STREAM)
serverSocket.bind(serverAddress)

blockDuration : float = float(sys.argv[2])
timeoutUser : float = float(sys.argv[3])


dbUsers = databaseClients(CREDENTIALS_FILE, blockDuration, MAX_PASSWORD_ATTEMPT)


class ClientThread(Thread):
    """
    Define multi-thread class for client
    This class would be used to define the instance for each connection from each client
    For example, client-1 makes a connection request to the server, the server will call
    class (ClientThread) to define a thread for client-1, and when client-2 make a connection
    request to the server, the server will call class (ClientThread) again and create a thread
    for client-2. Each client will be runing in a separate therad, which is the multi-threading
    """

    # create a class static list of all instances
    # the run alert() ?


    def __init__(self, clientAddress, clientSocket):
        Thread.__init__(self)
        self.clientAddress = clientAddress
        self.clientSocket : socket = clientSocket
        self.clientAlive = False
        
        print("===== New connection created for: ", clientAddress)
        self.clientAlive = True

        self.username : str = ""
    
    def sendNotif(self, msg):
        targetSockets : List[socket] = dbUsers.getNotifSockets(self.username)

        for sckt in targetSockets:
            sckt.sendall(assertPTRCLMsg(msg).encode())
            printSend(msg)

    def run(self):
        message = ''
        self.clientSocket.settimeout(timeoutUser)

        while self.clientAlive:
            try:
                # use recv() to receive message from the client
                data = self.clientSocket.recv(1024)
                message = data.decode()
                
                msgList = message.split()

                # if the message from client is empty, the client would be off-line then set the client as offline (alive=Flase)
                if message == '':
                    self.clientAlive = False
                    break
                
                # handle message from the client
                if message == assertPTRCLMsg('Auth_Login'):
                    self.process_login(message)

                elif msgList[0] == assertPTRCLMsg('Auth_UserInput_Username'):
                    self.process_recv_username(msgList)

                elif msgList[0] == assertPTRCLMsg('Auth_UserInput_Password'):
                    self.process_recv_password(msgList)

                elif msgList[0] == assertPTRCLMsg('Auth_UserInput_NewUser'):
                    self.process_new_user(msgList)
                
                elif msgList[0] == assertPTRCLMsg('UserCommand_Logout'):
                    self.process_logout(msgList)
                    break
                
                elif msgList[0] == assertPTRCLMsg('UserCommand_Message'):
                    self.process_send_msg(msgList)

                elif msgList[0] == assertPTRCLMsg('UserCommand_Broadcast'):
                    self.process_broadcast(msgList)
                
                elif msgList[0] == assertPTRCLMsg('UserCommand_Whoelse') or \
                    msgList[0] == assertPTRCLMsg('UserCommand_Whoelsesince'):
                    self.process_whoelse(msgList)

                elif msgList[0] == assertPTRCLMsg('UserCommand_Block'):
                    self.process_block(msgList, True)

                elif msgList[0] == assertPTRCLMsg('UserCommand_Unblock'):
                    self.process_block(msgList, False)
                
                elif msgList[0] == assertPTRCLMsg("UserCommand_Req_OfflineMsgs"):
                    self.process_offline_msgs(msgList)

                #region P2P
                elif msgList[0] == assertPTRCLMsg("UserCommand_Private_Init"):
                    self.process_send_msg(msgList)

                elif msgList[0] == assertPTRCLMsg("UserCommand_Private_RespAccept"):
                    self.process_send_msg(msgList, True)

                elif msgList[0] == assertPTRCLMsg("UserCommand_Private_RespReject"):
                    self.process_send_msg(msgList, False)

                #endregion

                elif msgList[0] == assertPTRCLMsg('Sink'):
                    continue

                else:
                    print("[recv] " + message)
                    print("[send] Cannot understand this message")
                    message = 'Cannot understand this message'
                    self.clientSocket.send(message.encode())

            except timeout as e:
                
                msgList = ["UserCommand_Logout", self.username]

                if (self.username == ""):
                    message = assertPTRCLMsg("Ack_Logout Login Timeout")
                    
                else:
                    try:
                        self.process_logout(msgList, True)
                        break
                    
                    # not even logged in, still in the logging in process
                    except LookupError as e:
                        message = assertPTRCLMsg("Ack_Logout Login Timeout")

                printSend(message)
                self.clientSocket.send(message.encode())
                self.clientAlive = False

        print("===== the user disconnected - ", clientAddress)

    """
        You can create more customized APIs here, e.g., logic for processing user authentication
        Each api can be used to handle one specific function, for example:
        def process_login(self):
            message = 'user credentials request'
            self.clientSocket.send(message.encode())
    """

    def process_login(self, clientMsg):
        printRecv(clientMsg)
        message = assertPTRCLMsg('Auth_Req_Username')
        printSend(message)
        self.clientSocket.send(message.encode())

    def process_recv_username(self, msgList : List[str]):
        '''
        If user does not exist, then create a new user. If user exists ask for password.
        '''
        printRecv(' '.join(msgList))

        try:
            # need to check user isn't blocked
            if dbUsers.isUserBlocked(msgList[1]):
                message = assertPTRCLMsg(f'Auth_Invalid_Block {msgList[1]} WrongPassword')
                printSend(message)
                self.clientSocket.send(message.encode())
                return
                
            elif dbUsers.isUserLoggedIn(msgList[1]):
                message = assertPTRCLMsg(f'Auth_Invalid_Block {msgList[1]} AlreadyLogged')
                printSend(message)
                self.clientSocket.send(message.encode())
                return

            self.username = msgList[1]

            message = assertPTRCLMsg('Auth_Req_Password ' + msgList[1])
            printSend(message)
        
        # User not in db
        except LookupError as e:
            self.username = msgList[1]
            message = assertPTRCLMsg('Auth_Req_NewUser ' + msgList[1])
            printSend(message)

        self.clientSocket.send(message.encode())
    
    def process_recv_password(self, msgList : List[str]):
        '''
        Should only be invoked by Auth_UserInput_Password. Its when an existing
        username has been entered, attempt to login.
        '''
        printRecv(' '.join(msgList))

        # protocolMsg [password]
        assert(len(msgList) == 2)
        message = dbUsers.logUser(self.username, msgList[1], self.clientSocket)

        if message.split(' ')[0] == assertPTRCLMsg("Auth_Valid"):
            self.sendNotif(f"NOTIF {self.username} logged in at {datetime.datetime.now()}")

        printSend(message)
        self.clientSocket.send(message.encode())

    def process_new_user(self, msgList : List[str]):

        printRecv(' '.join(msgList))

        dbUsers.registerUser(msgList[1], msgList[2], self.clientSocket)

        # Alert other users new user registered and is now online
        self.sendNotif(f"NOTIF {msgList[1]} logged in at {datetime.datetime.now()}")

        message = assertPTRCLMsg("Auth_Valid " + msgList[1])
        printSend(message)
        self.clientSocket.send(message.encode())
    
    def process_logout(self, msgList : List[str], timeout : bool = False):
        printRecv(" ".join(msgList))
        dbUsers.logoutUser(msgList[1])

        self.sendNotif(f"NOTIF {msgList[1]} logged out at {datetime.datetime.now()}")

        if timeout:
            message = assertPTRCLMsg(f"Ack_Logout Timeout {msgList[1]}")
        else:
            message = assertPTRCLMsg("Ack_Logout")
        
        printSend(message)
        self.clientSocket.send(message.encode())

    def process_send_msg(self, msgList : List[str], startPrivateAccept : Optional[bool] = None):
        '''
        Used by 
        -   UserCommand_Broadcast
        -   UserCommand_Message
        -   UserCommand_Private_Init

        Used to send message from one client to another via the server.

        Precondition:
        -   If used by braodcast, ensured that not called for users that are offline.

        Send <message> to <user> through the server. 
        
        If the user is
        online then deliver the message immediately, else store the
        message for offline delivery. 
        
        If <user> has blocked Yoda, then a
        message to that effect should be displayed for Yoda. 
        
        If the <user> is invalid or is self (Yoda) then an appropriate error message
        should be displayed. 
        
        The <message> used in our tests will be a few words at most
        '''

        if msgList[0] != assertPTRCLMsg("UserCommand_Broadcast"):
            printRecv(' '.join(msgList))

        message = ""
        sendSocket : Optional[socket] = None

        # can't message self
        if msgList[1] == self.username:

            # don't want to print error msg for when using this function for broadcast
            if msgList[0] == assertPTRCLMsg("UserCommand_Broadcast"):
                return

            elif msgList[0] == assertPTRCLMsg("UserCommand_Private_Init"):
                message = assertPTRCLMsg(f"UserCommand_Invalid {self.username} cannot start private with self.")

            else:
                message = assertPTRCLMsg(f"UserCommand_Invalid {self.username} cannot message to self.")
            sendSocket = self.clientSocket

        else:
            try:
                # want the message to contain msgList[2:] since you don't want to include 
                # the UserMessage prtcl header or the name of the target receiver
                otherUser = dbUsers.findUserData(msgList[1])

                # User is blocked by otheruser
                if otherUser.getBlockList().count(self.username):
                    sendSocket = self.clientSocket
                    message = assertPTRCLMsg(f"UserCommand_Invalid {msgList[1]} has blocked you.")

                # user is valid and is not blocked
                else:
                    
                    # We don't want to send offlines, instead store them
                    if not otherUser.getIsOnline()[0]:
                        
                        # if for starting private
                        if msgList[0] == assertPTRCLMsg("UserCommand_Private_Init"):
                            sendSocket = self.clientSocket
                            message = assertPTRCLMsg(f"UserCommand_Invalid {msgList[1]} " +
                            "can't have private session since currently offline.")

                        else:

                            # want the message to instead store the sender rather than the recipient
                            msgList[1] = self.username

                            otherUser.addOfflineMsg(msgList)
                            print("[Offline Msg Stored] " + " ".join(msgList))
                            return

                    # message actually goes through
                    else:
                        sendSocket = otherUser.getSocket()
                        
                        # if for starting private
                        if msgList[0] == assertPTRCLMsg("UserCommand_Private_Init"):
                            # replcae name with the initiater
                            msgList[1] = self.username
                            message = assertPTRCLMsg(" ".join(msgList))
                        
                        # Forwards it to the other party
                        elif not startPrivateAccept is None:
                            message = assertPTRCLMsg(" ".join(msgList))

                        else:
                            message = assertPTRCLMsg(f"Message_From {self.username} : " + 
                            ' '.join(msgList[2:]))

            except LookupError as e:
                sendSocket = self.clientSocket
                message = assertPTRCLMsg(f"UserCommand_Invalid {msgList[1]} " + \
                    "is not a valid user in the database.")

                if msgList[0] == assertPTRCLMsg("UserCommand_Private_Init"):
                    message += " Can't start private session."

                

        printSend(message)
        assert(not sendSocket is None)
        sendSocket.send(message.encode())

    def process_offline_msgs(self, msgsList : List[str]):
        printRecv(" ".join(msgsList))

        userMsgBank : List[MsgBank] = dbUsers.findUserData(self.username).getOfflineMsgBank()
        
        for bank in userMsgBank:
            otherUsername, msgs = bank.getMsgList()

            for msg in msgs:
                message = assertPTRCLMsg(f"ServerResp_OfflineMsgs {otherUsername} : {msg}")
                printSend(message)
                self.clientSocket.send(message.encode())
                time.sleep(0.2)

            bank.clearBank()

        pass


    def process_broadcast(self, msgList : List[str]):
        printRecv(" ".join(msgList))

        # msgList in the format of UserCommand_Broadcast msg1 msg2 msg3 ...
        message : List[str] = msgList[1:]

        for userObj in dbUsers.db:
            
            # don't need to worry about blocking, already handled in process_send_msg
            if userObj.getIsOnline()[0]:
                
                sendList = ["UserCommand_Broadcast", userObj.getUsername()]
                sendList.extend(message)

                self.process_send_msg(sendList)

    def process_whoelse(self, msgList : List[str]):
        printRecv(" ".join(msgList))
        message = ""

        dbResp : List[str] = []

        if msgList[0] == assertPTRCLMsg("UserCommand_Whoelsesince"):
            dbResp = dbUsers.whoelse(msgList[1], float(msgList[2]))
            message = assertPTRCLMsg("ServerResp_Whoelsesince ")
        else:
            dbResp = dbUsers.whoelse(msgList[1])
            message = assertPTRCLMsg("ServerResp_Whoelse ")
        
        # Join the list, and check if no users were found
        message += "NoUsersFound" if dbResp == [] else " ".join(dbResp)

        printSend(message)
        self.clientSocket.send(message.encode())

    def process_block(self, msgList : List[str], toBlock : bool):
        printRecv(" ".join(msgList))
        message : str = ""

        blockPhrase : str = ""
        if toBlock:
            blockPhrase = "block"
        else:
            blockPhrase = "unblock"

        # If try to block self
        if msgList[1] == msgList[2]:
            message = assertPTRCLMsg(f"UserCommand_Invalid {msgList[1]} can't {blockPhrase} self.")
        else:
            
            targetExists = True

            # in the format of UserCommand_Block caller target
            try:
                dbUsers.findUserData(msgList[2])

            # target doesn't exist
            except LookupError as e:
                message = assertPTRCLMsg(f"UserCommand_Invalid {msgList[2]} not found in database" + \
                    f" and can't be {blockPhrase}ed.")
                targetExists = False
            
            if targetExists:
                try:
                    # grab the callers userData and set the block on the target's username
                    dbUsers.findUserData(msgList[1]).setBlock(msgList[2], toBlock)
                    message = assertPTRCLMsg(f"Ack_Block {blockPhrase} {msgList[2]}")
                except ValueError as e:
                    if toBlock:
                        message = assertPTRCLMsg(f"UserCommand_Invalid {msgList[2]} is already blocked")
                    else:
                        message = assertPTRCLMsg(f"UserCommand_Invalid {msgList[2]} is not currently blocked and can't be unblocked")

        printSend(message)
        self.clientSocket.send(message.encode())


if __name__ == "__main__":
    print("\n===== Server is running =====")
    print("===== Waiting for connection request from clients...=====")


    while True:
        serverSocket.listen()
        clientSockt, clientAddress = serverSocket.accept()
        clientThread = ClientThread(clientAddress, clientSockt)
        clientThread.start()