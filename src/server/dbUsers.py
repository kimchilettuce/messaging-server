from typing import List, Optional
from src.Helper import assertPTRCLMsg
from socket import *
import datetime


class databaseClients:
    def __init__(self, fileCredentials : str, blockDuration, maxPasswordAttempt) -> None:
        '''
        Populates the db based on a txt file.
        '''
        self.db : List[userData] = []
        self.fileCredentials = fileCredentials
        self.MAX_PASSWORD_ATTEMPT = maxPasswordAttempt
        self.blockDuration : float = blockDuration

        lines = []
        with open(fileCredentials) as f:
            lines = f.readlines()

        for line in lines:
            lineSplit = line.split(' ')
            self.db.append(userData(lineSplit[0], lineSplit[1].strip()))
    

    def isUserBlocked(self, username) -> bool:
        _, _, blocked = self.findUserData(username).getLoginAttempt(self.blockDuration)
        return blocked

    def isUserLoggedIn(self, username : str):
        return self.findUserData(username).getIsOnline()[0]

    def findUserData(self, username):
        '''
        Raises a LookupError
        '''
        for data in self.db:

            dataUsername, _ , _ = data.getLoginAttempt(self.blockDuration)
            if username == dataUsername:
                return data

        raise LookupError

    def logUser(self, username, password, clientSocket : socket) -> str:
        '''
        return the protocol message {Auth_Valid, Auth_Invalid_Block, Auth_Invalid_Password}
        '''

        # socket will get set regardless of wether invalid or valid.
        # Doesn't rly matter, since the socket will only be accessed after checking that
        # isLoggedIn is true.
        userObj : userData = self.findUserData(username)
        userObj.setSocket(clientSocket)

        return userObj.loginUser(password, self.MAX_PASSWORD_ATTEMPT)
    
    def registerUser(self, username, password, clientSocket : socket) -> None:

        newUser = userData(username, password, clientSocket)
        newUser.setLogin()

        self.db.append(newUser)

        with open(self.fileCredentials, "a") as f:
            f.write('\n' + username + ' ' + password)

        return

    def logoutUser(self, username):
        self.findUserData(username).setLogOut()

    def whoelse(self, username, since : float = None) -> List[str]:
        '''
        Username is username of the caller itself

        Returns a list of usernames
        '''

        ans : List[str] = []

        sinceTime : Optional[datetime.datetime] = None
        if not since is None:
            sinceTime = datetime.datetime.now() - datetime.timedelta(seconds = since)

        for otherUser in self.db:

            active, loginStamp, logoutStamp = otherUser.getIsOnline()
            otherUserblockList = otherUser.getBlockList()

            # skip any users that specifically blocked username
            # or if referring to self
            if otherUserblockList.count(username) or otherUser.getUsername() == username:
                continue

            # Implementation of whoelsesince
            if not since is None:

                # assert(not stamp is None)
                assert(not loginStamp == None)
                assert(not sinceTime == None)

                # only add if login is after sinceTime
                # or if logout is after sinceTime
                if loginStamp > sinceTime:               
                    ans.append(otherUser.getUsername())

                # already deemed to be active, and logged in before sinceTime
                # if before sinceTime, then it logged out already
                if not logoutStamp == None and loginStamp < sinceTime:
                    continue
                else:
                    ans.append(otherUser.getUsername())

            # implementation of whoelse
            else:
                if active:
                    ans.append(otherUser.getUsername()) 
        
        return ans

    def getNotifSockets(self, callerUsername) -> List[socket]:
        '''
        - Need to not include blocked users
        - Users need to be logged in
        '''

        ans : List[socket] = []

        blockList : List[str] = self.findUserData(callerUsername).getBlockList()

        for dataObj in self.db:
            if (dataObj.getIsOnline()[0] 
                and not dataObj.getUsername() == callerUsername 
                and not blockList.count(dataObj.getUsername())
            ):
                ans.append(dataObj.getSocket())

        return ans

class MsgBank:
    def __init__(self, usernameFrom):
        self.__usernameFrom : str = usernameFrom
        self.__msgs : List[str] = []
    
    def clearBank(self):
        self.__msgs = []
    
    def addMsg(self, msg : str):
        self.__msgs.append(msg)
    
    def getMsgList(self):
        return (self.__usernameFrom, self.__msgs)


class userData:
    def __init__(self, username, password, clientSocket : socket = None) -> None:
        
        self.__username : str = username
        self.__password : str = password
        self.__clientSocket : Optional[socket] = clientSocket

        self.__loginAttempt : int = 0
        self.__loginBlocked : bool = False
        self.__blockedTimeStamp : Optional[datetime.datetime] = None

        self.__isLoggedIn : bool = False
        self.__loginStamp : Optional[datetime.datetime] = None
        self.__logoutStamp : Optional[datetime.datetime] = None

        self.__blockList : List[str] = []

        # [{"Peter" : [msg1, msg]}, {"Anna", [msg2, msg3]}]
        self.__offlineMsgBank : List[MsgBank] = []

    def prettyPrint(self):
        print(f"[Username : {self.__username}] [Password : {self.__password}]")


    def loginUser(self, password, maxAttempt) -> str:
        '''
        Adds +1 to login attempt.
        Returns
            - the protocol msg
        '''
        
        # correct password
        if password == self.__password:
            self.setLogin()
            return assertPTRCLMsg('Auth_Valid ' + self.__username)

        # For wrong password, need the minus 1
        if self.__loginAttempt == maxAttempt - 1:
            self.__loginBlocked = True
            self.__blockedTimeStamp = datetime.datetime.now()

            return assertPTRCLMsg(f"Auth_Invalid_Block {self.__username} WrongPassword")

        else:
            self.__loginAttempt += 1
            return assertPTRCLMsg("Auth_Invalid_Password")

    #region Getter and Setters
    def getLoginAttempt(self, blockDuration):
        '''
        Not to be used as a getter.
        username, loginAttemptCount, blocked
        '''

        # free user from being blocked
        if self.__loginBlocked and not self.__blockedTimeStamp == None:
            if datetime.datetime.now() > self.__blockedTimeStamp + datetime.timedelta(seconds = blockDuration):
                self.__loginAttempt = 0
                self.__loginBlocked = False
                self.__blockedTimeStamp = None

        return (self.__username, self.__loginAttempt, self.__loginBlocked)

    def getUsername(self):
        return self.__username

    def getBlockList(self):
        return self.__blockList
    
    def getIsOnline(self):
        '''
        isLoggedIn, loginStamp (time of logging in)
        '''
        return self.__isLoggedIn, self.__loginStamp, self.__logoutStamp
    
    def getSocket(self) -> socket:
        if self.__clientSocket is None:
            raise LookupError("Socket is not initialised.")
        else:
            return self.__clientSocket

    def getOfflineMsgBank(self):
        return self.__offlineMsgBank

    def setLogin(self):
        '''
        Used to set attributes for when user logs in
        '''
        self.__loginStamp = datetime.datetime.now()
        self.__isLoggedIn = True

    def setLogOut(self):
        self.__isLoggedIn = False
        self.__logoutStamp = datetime.datetime.now()

        self.__loginAttempt = 0
        self.__loginBlocked = False
        self.__blockedTimeStamp = None

    def setSocket(self, clientSocket : socket):
        self.__clientSocket = clientSocket

    def setBlock(self, targetUserToBlock : str, toBlock : bool):
        '''
        Raises Value Error

        Precondtion:
        -   the user to block exists in db
        '''

        hasFoundUser = self.__blockList.count(targetUserToBlock)

        if toBlock and not hasFoundUser:
            self.__blockList.append(targetUserToBlock)
        elif not toBlock and hasFoundUser:
            self.__blockList.remove(targetUserToBlock)
        else:
            raise ValueError
    
    def addOfflineMsg(self, msgList : List[str]):

        # in the format of:
        # Message_From kinzey : hi
        # UserCommand_Message kinzey hi this is my msg

        print(msgList)

        for bank in self.__offlineMsgBank:

            # Has found the specific otherUser Msgbank 
            if bank.getMsgList()[0] == msgList[1]:
                bank.addMsg(" ".join(msgList[2:]))
                return
        
        # else adds a new bank
        newBank = MsgBank(msgList[1])
        newBank.addMsg(" ".join(msgList[2:]))
        self.__offlineMsgBank.append(newBank)
        return
    
    #endregion

if __name__ == "__main__":
    print(datetime.datetime.now())
    print(datetime.datetime.now() + datetime.timedelta(seconds = 3))

    x = ("hi", 3)
    print(x[1])
    
    print(x == None)
