PROTOCOL_MSGS = [
    "Auth_Login",

    "Auth_UserInput_Username",
    "Auth_UserInput_Password",
    "Auth_UserInput_NewUser",

    "Auth_Req_Username",
    "Auth_Req_NewUser",
    "Auth_Req_Password",

    "Auth_Valid",
    "Auth_Invalid_Password",
    "Auth_Invalid_Block",

    "NOTIF",
    
    "UserCommand_Req_OfflineMsgs",

    "UserCommand_Logout",
    "UserCommand_Message",
    "UserCommand_Broadcast",

    "UserCommand_Invalid",
    "UserCommand_Whoelse",
    "UserCommand_Whoelsesince",
    "UserCommand_Block",
    "UserCommand_Unblock",

    "UserCommand_Private_Init",
    "UserCommand_Private_RespAccept",
    "UserCommand_Private_RespReject",

    "Message_From",
    "ServerResp_Whoelse",
    "ServerResp_Whoelsesince",
    "ServerResp_OfflineMsgs",

    "Ack_Logout",
    "Ack_Block",

    "Sink"
]

USER_COMMANDS = [
    "message",
    "broadcast",
    "whoelse",
    "whoelsesince",
    "block",
    "unblock",
    "logout",

    "startprivate",
    "private",
    "stopprivate",

    "y"
]

debug = True

def printRecv(msg : str):
    if debug:
        print("[recv] " + msg)

def printSend(msg : str):
    if debug:
        print("[send] " + msg)

def printPrompt(msg : str):
    print("~ "+ msg)

def inputPrompt(msg : str) -> str:
    return input("~ " + msg)

def assertPTRCLMsg(protocolMsg):

    stringList = protocolMsg.split()
    assert(PROTOCOL_MSGS.count(stringList[0]))
    return protocolMsg

def assertUserCommand(userCommand):

    stringList = userCommand.split()
    assert(USER_COMMANDS.count(stringList[0]))
    return userCommand

def findUserPasswordInCredentials(username) -> str:
    '''
    Returns password if found, else returns empty string
    '''
    lines = []
    with open('credentials.txt') as f:
        lines = f.readlines()

    for line in lines:
        if (line.split()[0] == username): return line.split()[1]

    return ""


if __name__ == "__main__":
    print('hi')
    print(findUserPasswordInCredentials("hans"))
    print(findUserPasswordInCredentials("r2d2"))
    print(findUserPasswordInCredentials("Kinzey"))





