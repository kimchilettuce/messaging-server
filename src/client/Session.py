from socket import *
from threading import Thread
from typing import Dict, Optional
from src.Helper import assertPTRCLMsg, inputPrompt, printPrompt, printRecv
import src.client.ClientHandler as ClientHandler, src.client.p2pConnections as p2pConnections


def startSession(clientSocket : socket, username : str):
    
    # start the thread to get commands
    ClientHandler.process_get_command(clientSocket, username)

    # start the thread to listen 
    # listenThead = p2pConnections.p2pListener()
    # listenThead.start()

    # requests messages that were stored offline
    sendToServerMsg = assertPTRCLMsg("UserCommand_Req_OfflineMsgs")
    clientSocket.send(sendToServerMsg.encode())

    # Used as a helper for offline messages from multiple users
    previousMsgFrom : str = ""

    while True:
        data = clientSocket.recv(1024)
        message = data.decode()

        msgList = message.split(" ")

        if msgList[0] == assertPTRCLMsg("NOTIF"):
            printRecv(message)
            printPrompt(f"Notification: {' '.join(msgList[1:])}")

        elif msgList[0] == assertPTRCLMsg("Ack_Logout"):
            printRecv(message)

            if len(msgList) > 1 and msgList[1].lower() == "timeout":
                printPrompt(f"{username} is being logged out for inactivity.")
                
            print("\n ========== Session Closing =========== \n")
            return
        
        elif msgList[0] == assertPTRCLMsg("Message_From"):
            printRecv(message)
            printPrompt(' '.join(msgList[1:]))
            print()
            printPrompt("Enter command: ")

        elif msgList[0] == assertPTRCLMsg("UserCommand_Invalid"):
            printRecv(message)
            printPrompt("Invalid command: " + ' '.join(msgList[1:]) + '\n')
            printPrompt("Enter command: ")

        elif msgList[0] == assertPTRCLMsg("UserCommand_Private_Init"):
            printRecv(message)

            printPrompt(f"{msgList[1]} wants to start a private peer to peer connection.")
            printPrompt("Do you accept the connection? (y/n)")

            ClientHandler.UserInputThread.sendTo = msgList[1]
            ClientHandler.UserInputThread.userInputStartPrivate = True

        elif msgList[0] == assertPTRCLMsg("UserCommand_Private_RespAccept"):
            printRecv(message)

            # msgList[1] the otheruser username
            # msgList[2] the connecting port num
            p2pSocket = socket(AF_INET, SOCK_STREAM)

            # build connection with the server and send message to it
            p2pSocket.connect(("localhost", int(msgList[2])))
            p2pConnections.connections[msgList[1]] = p2pSocket

            printPrompt(f"Connection with {msgList[1]} established")

        elif msgList[0] == assertPTRCLMsg("UserCommand_Private_RespReject"):
            printRecv(message)

            printPrompt(f"Peer to peer connection with {msgList[1]} is rejected.")
            

        elif msgList[0] == assertPTRCLMsg("ServerResp_Whoelse"):
            printRecv(message)

            if msgList[1] == "NoUsersFound":
                printPrompt("There are no other valid users currently online.")
            else:
                print("\n~ Listed below are the other online users")
                for string in msgList[1:]:
                    printPrompt(f"\t {string}")
                print()

            printPrompt("Enter command: ")

        elif msgList[0] == assertPTRCLMsg("ServerResp_Whoelsesince"):
            printRecv(message)

        elif msgList[0] == assertPTRCLMsg("ServerResp_OfflineMsgs"):
            printRecv(message)

            if not previousMsgFrom == msgList[1]:
                print(f"\nOffline messages from {msgList[1]}")

            printPrompt(" ".join(msgList[1:]))

            previousMsgFrom = msgList[1]


        elif msgList[0] == assertPTRCLMsg("Ack_Block"):
            printRecv(message)
            printPrompt(f"{msgList[2]} is successfully {msgList[1]}ed.\n")

        else:
            print("\nSession did not understand msg")
            printRecv(message + '\n')
            printPrompt("Enter command: ")

