from socket import socket, AF_INET, SOCK_STREAM
from threading import Thread
from typing import List, Optional
from src.Helper import printSend, printPrompt, printRecv, inputPrompt, assertPTRCLMsg, assertUserCommand
from time import sleep
import src.client.p2pConnections as p2pConnections


class UserInputThread(Thread):

    TYPES = ["username", "password", "command"]

    userInputStartPrivate : bool = False
    sendTo : str = ""

    def __init__(self, clientSocket, type : str, username : str = ""):
        Thread.__init__(self)
        self.setDaemon(True)
        self.__socket = clientSocket
        self.__type = type
        self.__username = username
        assert(self.TYPES.count(type))
    
    def run(self):

        if self.__type == "username":
            self.getInput("Enter username: ", "Auth_UserInput_Username")
        elif self.__type == "password":
            self.getInput("Enter password: ", "Auth_UserInput_Password")
        elif self.__type == "command":
            self.getCommand("Enter command: ")

    def getInput(self, promptMsg : str, prtclMsg : str):
        print("\n~" + promptMsg)
        message = assertPTRCLMsg(prtclMsg + " ") + input()
        printSend(message)
        self.__socket.sendall(message.encode())
    
    def getCommand(self, promptMsg):
        while True:
            printPrompt(promptMsg)
            userIn = input()

            userInList = userIn.split(" ")
            message = ""

            if userInList[0] == assertUserCommand("logout"):
                message = "UserCommand_Logout " + self.__username
            
            elif userInList[0] == assertUserCommand("message"):
                # remove the word "message"
                userInList.remove(userInList[0])
                
                # send the rest to the server
                message = assertPTRCLMsg("UserCommand_Message " + ' '.join(userInList))

            elif userInList[0] == assertUserCommand("whoelse"):                
                message = assertPTRCLMsg("UserCommand_Whoelse " + self.__username)

            elif userInList[0] == assertUserCommand("whoelsesince"):                
                message = assertPTRCLMsg("UserCommand_Whoelsesince " + 
                self.__username + " " + userInList[1])

            elif userInList[0] == assertUserCommand("block"):
                # In the format of 
                # block yoda kinzey (where yoda wants to block kinzey)
                message = assertPTRCLMsg("UserCommand_Block " + 
                self.__username + " " + userInList[1])

            elif userInList[0] == assertUserCommand("unblock"):
                # In the format of 
                # unblock yoda kinzey (where yoda wants to unblock kinzey)
                message = assertPTRCLMsg("UserCommand_Unblock " + 
                self.__username + " " + userInList[1])


            elif userInList[0] == assertUserCommand("broadcast"):
                message = assertPTRCLMsg("UserCommand_Broadcast " + " ".join(userInList[1:]))


            elif userInList[0] == assertUserCommand("startprivate"):
                message = assertPTRCLMsg("UserCommand_Private_Init " + " ".join(userInList[1:]))

            elif userInList[0] == assertUserCommand("y") and self.userInputStartPrivate == True:
                printPrompt(f"Connection with {self.sendTo} accepted.\n")

                # might need to fix this, causing problem with da thread
                # welcomeThread = p2pConnections.AcceptConnection(self.sendTo)
                # welcomeThread.start()

                # make sure welcomePortNum has been generated
                while p2pConnections.welcomePortNum == 0:
                    sleep(0.1)

                message = assertPTRCLMsg(f"UserCommand_Private_RespAccept {self.sendTo} " +
                f"{p2pConnections.welcomePortNum}")

                self.userInputStartPrivate = False

            else:
                if self.userInputStartPrivate == True:
                    printPrompt("Connection rejected.\n")
                    message = assertPTRCLMsg(f"UserCommand_Private_RespReject {self.sendTo}")
                    self.userInputStartPrivate = False
                else:
                    message = assertPTRCLMsg("Sink unrecognised command")
                    printPrompt(f"[{' '.join(userInList)}] is not a recognised command.\n")

            printSend(message)
            self.__socket.sendall(message.encode())

inputThread : Optional[UserInputThread] = None

def process_req_username(clientSocket : socket, serverMsg):
    printRecv(serverMsg)

    inputThread = UserInputThread(clientSocket, "username")
    inputThread.start()
    return

def process_req_password(clientSocket : socket, serverMsg : str):
    printRecv(serverMsg)

    if serverMsg.split(' ')[0] == assertPTRCLMsg("Auth_Invalid_Password"):
        printPrompt("Wrong password. Please try again.")

    inputThread = UserInputThread(clientSocket, "password")
    inputThread.start()
    return

def process_get_command(clientSocket : socket, username : str):

    inputThread = UserInputThread(clientSocket, "command", username)
    inputThread.start()
    return


#endregion

def process_new_user(clientSocket : socket, serverMsg : List[str]) -> bool:
    '''
    Returns whether or not they want to make a new user.
    '''
    printRecv(' '.join(serverMsg))

    printPrompt("This user is currently not registered.")
    userResp = inputPrompt(f"Would you like to register [{serverMsg[1]}] as a new user? (y/n): ")

    if userResp == "y":
        print("--- New User Registration ---")
        password = input("Enter new password: ")
        
        message = assertPTRCLMsg("Auth_UserInput_NewUser " + serverMsg[1] + ' ' + password)
        printSend(message)

        clientSocket.sendall(message.encode())

        return True
    else:
        return False



