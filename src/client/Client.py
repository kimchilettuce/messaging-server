"""
    Python 3
    Usage: python3 TCPClient3.py localhost 12000
    coding: utf-8
    
    Author: Wei Song (Tutor for COMP3331/9331)
"""
from socket import *
from src.Helper import printRecv, printSend, printPrompt, assertPTRCLMsg
from src.client.ClientHandler import process_req_username, process_req_password, process_new_user
import sys, src.client.Session as Session



#Server would be running on the same host as Client
if len(sys.argv) != 2:
    print("\n===== Error usage, python3 TCPClient3.py SERVER_PORT ======\n");
    exit(0);

serverHost = "localhost"
serverPort = int(sys.argv[1])
serverAddress = (serverHost, serverPort)

# define a socket for the client side, it would be used to communicate with the server
clientSocket = socket(AF_INET, SOCK_STREAM)

# build connection with the server and send message to it
clientSocket.connect(serverAddress)

# ----------------------------------------------------------------

# Start off with sending a login request
printSend(assertPTRCLMsg("Auth_Login"))
clientSocket.sendall(assertPTRCLMsg("Auth_Login").encode())

while True:
    # message = input("===== Please type any messsage you want to send to server: =====\n")
    # clientSocket.sendall(message.encode())

    # receive response from the server
    # 1024 is a suggested packet size, you can specify it as 2048 or others
    data = clientSocket.recv(1024)
    receivedMessage = data.decode()

    msgList = receivedMessage.split()

    # parse the message received from server and take corresponding actions
    if receivedMessage == "":
        print("[recv] Message from server is empty!")

    elif receivedMessage == assertPTRCLMsg("Auth_Req_Username"):
        process_req_username(clientSocket, receivedMessage)
        continue

    elif msgList[0] == assertPTRCLMsg("Auth_Req_Password"):
        process_req_password(clientSocket, receivedMessage)
        continue

    elif msgList[0] == assertPTRCLMsg("Auth_Req_NewUser"):
        if process_new_user(clientSocket, msgList): continue
        break
    
    elif msgList[0] == assertPTRCLMsg("Auth_Valid"):
        printRecv(receivedMessage)
        print("\n ============== Logged In ================= \n")

        Session.startSession(clientSocket, msgList[1])
        break

    elif msgList[0] == assertPTRCLMsg("Auth_Invalid_Password"):
        process_req_password(clientSocket, receivedMessage)
        continue

    elif msgList[0] == assertPTRCLMsg("Auth_Invalid_Block"):
        printRecv(receivedMessage)
        if msgList[2] == "AlreadyLogged":
            printPrompt(f"{msgList[1]} is already logged in. Please try again later")
        else:
            printPrompt(f"{msgList[1]} is blocked. Please try to login later.")
        break
    
    # We don't want those who haven't logged in to received notifcations
    # Only want this to be handled in Session.py
    elif msgList[0] == assertPTRCLMsg("NOTIF"):
        # printRecv(receivedMessage)
        continue
    
    # Send by server when it wants to shutdown
    elif msgList[0] == assertPTRCLMsg("Ack_Logout"):
        printRecv(receivedMessage)
        print("\n ======= Authentication session timeout. Closing Session. ======= \n")
        break


    elif receivedMessage == "download filename":
        print("[recv] You need to provide the file name you want to download")

    else:
        print("[recv] Message makes no sense")
        print(receivedMessage)

# close the socket
clientSocket.close()