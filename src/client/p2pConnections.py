from typing import Dict
from threading import Thread
from socket import *

connections : Dict[str, socket] = {}
welcomePortNum : int = 0

class AcceptConnection(Thread):
    def __init__(self, otherUsername : str):
        Thread.__init__(self)
        self.daemon = True
        self.__otherusername = otherUsername

    def run(self):

        # Give it a random port number
        myAddress = ("localhost", 0)
        p2pSocket = socket(AF_INET, SOCK_STREAM)
        p2pSocket.bind(myAddress)

        welcomePortNum = p2pSocket.getsockname()[1]

        p2pSocket.listen()            
        clientSockt, _ = p2pSocket.accept()

        connections[self.__otherusername] = clientSockt
        
        # already accepted, can reset back to 0
        welcomePortNum = 0
        p2pSocket.close()

class p2pListener(Thread):

    def __init__(self):
        Thread.__init__(self)
        self.daemon = True
    
    def run(self):

        while True:
            if len(connections.values()) == 0:
                # print("DEBUG: here length 0")
                continue

            for skt in connections.values():
                data = skt.recv(1024)
                message = data.decode()

                print(f"DEBUG: p2p message {message}")