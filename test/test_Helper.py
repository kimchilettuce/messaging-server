from Helper import *

# Run using: 
# python3 -m pytest test

def test_usercredentials():
    assert findUserInCredentials("hans")
    assert findUserInCredentials("yoda")
    assert not findUserInCredentials("Kinzey")
    assert not findUserInCredentials("hAnS")

